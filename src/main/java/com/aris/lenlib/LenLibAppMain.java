package com.aris.lenlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.aris.lenlib.*" })
public class LenLibAppMain {

    public static void main(String[] args) {
        SpringApplication.run(LenLibAppMain.class, args);
    }

}
