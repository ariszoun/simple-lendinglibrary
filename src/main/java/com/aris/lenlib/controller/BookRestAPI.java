package com.aris.lenlib.controller;

import com.aris.lenlib.model.enums.Status;
import com.aris.lenlib.model.Book;
import com.aris.lenlib.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api")
public class BookRestAPI {

    @Autowired
    private BookService bs;

    //GET ALL BOOKS
    @GetMapping("books")
    public List<Book> getAllBooks() {
        return bs.findAll();
    }

    //GET BOOK BY TITLE
    @GetMapping("bookByTitle")
    public Book getBookByTitle(@RequestParam String title){
        Optional<Book> book = bs.findByTitle(title);
        return book.orElse(null);
    }

    //GET BOOKS BY STATUS
    @GetMapping("booksByStatus")
    public List<Book> getBooksByStatus(@RequestParam Status status){
        List<Book> book = bs.findByStatus(status);
        return book.isEmpty()?null:book;
    }

    //ADD NEW BOOK
    @PostMapping("add/book")
    public Book addBook(@RequestBody Book book) {
        return bs.addBook(book);
    }

    //UPDATE BOOK
    @PutMapping("edit/book")
    public Book editBook(@RequestBody Book book, @RequestParam String title){
        return bs.editBook(book, title);
    }

    @DeleteMapping("delete/book")
    public void deleteBook(@RequestParam String title){
        bs.deleteBookByTitle(title);
    }
}
