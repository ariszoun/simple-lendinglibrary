package com.aris.lenlib.controller;

import com.aris.lenlib.model.Lending;
import com.aris.lenlib.service.LendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class LendingRest {

    @Autowired
    private LendingService ls;

    //GET ALL LENDINGS
    @GetMapping("lendings")
    public List<Lending> getLendings(){
        return ls.findAll();
    }

    //ADD LENDING
    @PostMapping("add/lending")
    public Lending addLending(@RequestBody Lending lending) {
        return ls.addLending(lending);
    }

    @PutMapping("edit/lending")
    public Lending editLending(@RequestBody Lending lending, @RequestParam Long id){
        return ls.editLending(lending, id);
    }


}
