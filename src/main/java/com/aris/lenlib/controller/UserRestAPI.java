package com.aris.lenlib.controller;

import com.aris.lenlib.model.User;
import com.aris.lenlib.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api")
public class UserRestAPI {

    @Autowired
    private UserService us;

    //GET ALL USERS
    @GetMapping("users")
    public List<User> getAllUsers(){
        return us.findAll();
    }

    //GET USER BY USERNAME
    @GetMapping("userByUsername") //?username=maria
    public User getUserByUsername(@RequestParam String username){
        User user = us.findByUsername(username);
        return user;
    }

    //ADD USER
    @PostMapping("add/user")
    public User addUser(@RequestBody User user) {
        return us.addUser(user);
    }

    //EDIT USER
    @PutMapping("edit/user")
    public User editUser(@RequestBody User user, @RequestParam String username){
        return us.editUser(user, username);
    }

    @DeleteMapping("delete/user")
    public void deleteUser(@RequestParam String username){
        us.deleteUserByUsername(username);
    }

}
