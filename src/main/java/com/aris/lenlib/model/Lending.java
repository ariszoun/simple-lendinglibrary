package com.aris.lenlib.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CollectionId;

import javax.persistence.*;
import java.util.stream.Stream;

@Entity
@Table(name = "Lending")
public class Lending {

    @Id
    @Column(name = "lid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonManagedReference
    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "uid")
    private User user;

    @JsonManagedReference
    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "bid")
    private Book book;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "return_date")
    private String returnDate;

    public Lending() {
    }

    public Lending(User user, Book book, String startDate, String returnDate) {
        this.user = user;
        this.book = book;
        this.startDate = startDate;
        this.returnDate = returnDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }
}
