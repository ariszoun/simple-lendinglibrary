package com.aris.lenlib.model.enums;

public enum Status {
    AVAILABLE,
    RENTED
}
