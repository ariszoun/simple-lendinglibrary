package com.aris.lenlib.repository;

import com.aris.lenlib.model.enums.Status;
import com.aris.lenlib.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findAll();

    //represent null with absent value
    Optional<Book> findByTitle(String title);

    List<Book> findByStatus(Status status);

    void deleteBookByTitle(String title);


}
