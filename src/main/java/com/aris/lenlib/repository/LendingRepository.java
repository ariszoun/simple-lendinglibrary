package com.aris.lenlib.repository;

import com.aris.lenlib.model.Lending;
import com.aris.lenlib.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LendingRepository extends JpaRepository<Lending, Long> {

    List<Lending> findAll();

    List<Lending> findByUser(User user);

    Optional<Lending> findById(String id);

    void deleteLendingById(Long id);

}
