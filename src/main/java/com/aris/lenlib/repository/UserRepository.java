package com.aris.lenlib.repository;

import com.aris.lenlib.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    User findByUsername(String username);

    void deleteUserByUsername(String username);

    //@Query("SELECT u FROM User u WHERE u.username = :username")
    // User getUserByUsername(@Param("username") String username);

}
