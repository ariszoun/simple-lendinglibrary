package com.aris.lenlib.service;


import com.aris.lenlib.model.enums.Status;
import com.aris.lenlib.model.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {

    List<Book> findAll();

    Book addBook(Book book);

    Book editBook(Book book, String title);

    void deleteBookByTitle(String title);

    //represent null with absent value
    Optional<Book> findByTitle(String title);


    List<Book> findByStatus(Status status);

}
