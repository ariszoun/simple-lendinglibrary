package com.aris.lenlib.service;

import com.aris.lenlib.model.enums.Status;
import com.aris.lenlib.model.Book;
import com.aris.lenlib.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository br;

    @Override
    public List<Book> findAll() {
        return br.findAll();
    }

    @Override
    public Book addBook(Book book) {
        return br.save(book);
    }

    @Override
    public Book editBook(Book book, String title) {
        Optional<Book> aBook = br.findByTitle(title);
        if (aBook.isPresent()) {
            aBook.get().setTitle(book.getTitle());
            aBook.get().setAuthor(book.getAuthor());
            aBook.get().setStatus(book.getStatus());
            return br.save(aBook.get());
        }
        return null;
    }

    @Override
    public void deleteBookByTitle(String title) {
        br.deleteBookByTitle(title);
    }

    @Override
    public Optional<Book> findByTitle(String title) {
        return br.findByTitle(title);
    }

    @Override
    public List<Book> findByStatus(Status status) {
        return br.findByStatus(status);
    }
}
