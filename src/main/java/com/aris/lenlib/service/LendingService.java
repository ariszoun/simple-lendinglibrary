package com.aris.lenlib.service;

import com.aris.lenlib.model.Lending;
import com.aris.lenlib.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface LendingService {

    List<Lending> findAll();

    List<Lending> findByUser(User user);

    Optional<Lending> findById(String id);

    Lending addLending(Lending lending);

    Lending editLending(Lending lending, Long id);

    void deleteLendingById(Long id);

}
