package com.aris.lenlib.service;

import com.aris.lenlib.model.Lending;
import com.aris.lenlib.model.User;
import com.aris.lenlib.repository.LendingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LendingServiceImpl implements LendingService {

    @Autowired
    private LendingRepository lr;

    @Override
    public List<Lending> findAll() {
        return lr.findAll();
    }

    @Override
    public List<Lending> findByUser(User user) {
        return lr.findByUser(user);
    }

    @Override
    public Optional<Lending> findById(String id) {
        return lr.findById(id);
    }

    @Override
    public Lending addLending(Lending lending) {
        return lr.save(lending);
    }

    @Override
    public Lending editLending(Lending lending, Long id) {
        Optional<Lending> aLending = lr.findById(id);
        return aLending.map(value -> lr.save(value)).orElse(null);
    }

    @Override
    public void deleteLendingById(Long id) {
        lr.deleteLendingById(id);
    }
}
