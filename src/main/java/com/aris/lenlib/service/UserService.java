package com.aris.lenlib.service;

import com.aris.lenlib.model.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findByUsername(String username);

    User addUser(User user);

    User editUser(User user, String username);

    void deleteUserByUsername(String username);

}
