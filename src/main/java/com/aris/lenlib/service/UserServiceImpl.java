package com.aris.lenlib.service;

import com.aris.lenlib.model.User;
import com.aris.lenlib.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository ur;

    @Override
    public List<User> findAll() {

        return ur.findAll();
    }

    @Override
    public User findByUsername(String username) {
        return ur.findByUsername(username);
    }

    @Override
    public User addUser(User user) {
        return ur.save(user);
    }

    @Override
    public User editUser(User user, String username) {
        User aUser = ur.findByUsername(username);
        if(aUser != null) {
            aUser.setName(user.getName());
            aUser.setLastname(user.getLastname());
            aUser.setUsername(user.getUsername());
            aUser.setPassword(user.getPassword());
//            aUser.get().setRole(user.getRole());
            return ur.save(aUser);
        }
        return null;
    }

    @Override
    public void deleteUserByUsername(String username) {
        ur.deleteUserByUsername(username);
    }
}
