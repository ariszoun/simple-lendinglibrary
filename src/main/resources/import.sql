
INSERT INTO USER (firstname, lastname, username, password) VALUES ('Aris', 'Zoun', 'aris', '$2a$04$ahjG0RPujs1davXEjO4tbOGEMpwRAhEnFkqzupdeOwQmEjtPAEeJS');
INSERT INTO USER (firstname, lastname, username, password) VALUES ('Maria', 'Tsili', 'maria', '$2a$12$x10gItlNj6hAGUFJJQ3kKuUyewVkDiq.jn/UuJHS.WD0yQ2MCgCD6');
INSERT INTO USER (firstname, lastname, username, password) VALUES ('Admin', 'Admin', 'admin', '$2a$10$IqTJTjn39IU5.7sSCDQxzu3xug6z/LPU6IF0azE/8CkHCwYEnwBX.');


INSERT INTO BOOK (author, status, title) VALUES ( 'Leo Tolstoy', 'AVAILABLE'        ,'War and Peace'      );
INSERT INTO BOOK (author, status, title) VALUES ( 'Arthur Conan Doyle', 'AVAILABLE' ,'The Grapes of Wrath');
INSERT INTO BOOK (author, status, title) VALUES ( 'Ernest Hemingway', 'RENTED'      ,'The Murderess'      );
INSERT INTO BOOK (author, status, title) VALUES (  'James Joyce', 'RENTED'          ,'Captain Michalis'   );
INSERT INTO BOOK (author, status, title) VALUES ( 'Arthur Conan Doyle', 'AVAILABLE' ,'Report to Greco'    );
INSERT INTO BOOK (author, status, title) VALUES ( 'Ernest Hemingway', 'RENTED'      ,'The Last Temptation');
INSERT INTO BOOK (author, status, title) VALUES ( 'James Joyce', 'AVAILABLE'        ,'Christ Recrucified' );
INSERT INTO BOOK (author, status, title) VALUES ( 'Leo Tolstoy', 'RENTED'           ,'Princess Isambo'   );

INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('1','2','12/12/2020','10/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('2','4','13/12/2020','11/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('1','3','14/12/2020','12/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('3','5','15/10/2020','13/07/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('1','2','16/12/2020','14/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('1','5','17/12/2020','18/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('3','2','18/12/2020','15/08/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('1','8','19/10/2020','17/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('1','2','12/12/2020','19/05/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('2','7','12/12/2020','10/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('2','2','05/11/2020','10/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('3','4','12/10/2020','10/02/2021');
INSERT INTO LENDING (uid, bid, start_date, return_date) VALUES ('3','8','12/12/2020','10/02/2021');

INSERT INTO `users_roles` (fuid, frid) VALUES ('1', '2');
INSERT INTO `users_roles` (fuid, frid) VALUES ('2', '2');
INSERT INTO `users_roles` (fuid, frid) VALUES ('3', '1');

INSERT INTO `roles` (`name`) VALUES ('LIBRARY_PERSONEL');
INSERT INTO `roles` (`name`) VALUES ('BOOK_LENDER');
